<?php
    echo "php standard tag!";
?>

<?= "short echo tag!"; ?>

<?
    echo "php short tag, not recommended!";
    /* short tag was not working so i changed
    short_open_tag = Off to short_open_tag = On
    in /etc/php/7.4/cli/php.ini file */
?>

<?php
    $name = "Farhan";
    $name = 22;
    if (gettype($name) == "string"):
?>
Hello Farhan!
<?php else: ?>
Must insert string!
<?php endif ?>

<?php 
    $a = 123.3;
    $b = 0x23;
    $c = "";
    $d = 12_432_54;
    $e = 17 / 3;
    $f = "Farhan";

    echo is_float($a);
    echo "\n";
    echo $b;
    echo "\n";
    echo var_dump((bool) $c);
    echo $d;
    echo "\n";
    echo PHP_INT_MIN;
    echo "\n";
    echo var_dump(round($e));
    echo var_dump((int) $f);

    $g = 'hello all\n $f';
    $h = "Hello $f";
    $hereDoc = <<<hd
    It is a heredoc form of string $f
    hd;
    $nowDoc = <<<'nd'
    It is a nowdoc form of string $f
    nd;
    $strConcatenation = "Greetings";

    echo $g;
    echo "\n";
    echo $h;
    echo "\n";
    echo $hereDoc;
    echo "\n";
    echo $nowDoc;
    echo "\n";
    echo $strConcatenation . " " . $f;
    echo "\n";

    $myArr = array("John", 22, "Jane", "name" => "Mary");
    echo $myArr[2] . " went to " . $myArr[0] . " and " . $myArr["name"] . "'s " . $myArr[1] . "th anniversary.";
    echo "\n";

    class Car {
        public $carName;
        public $carModel;
        function __construct ($carName, $carModel) {
            $this -> carName = $carName;
            $this -> carModel = $carModel;
        }

        function theCar() {
            echo $this -> carName . " " . $this -> carModel;
        }
    }

    $newCar = new Car ("Chevrolet", "Corvette");
    $newCar -> theCar();
    echo "\n";
    $myCar = &$newCar->carName;
    echo $myCar;
    echo "\n";

    echo $GLOBALS["b"];
    echo "\n";

    $$myCar = $newCar->carModel;
    echo "I Have a $myCar ${$myCar}.";
    echo "\n";

    $now = date("Y/m/d");
    echo $now;
    echo "\n";
?>
