<?php
// default arguments
function is_prime ($num = 7) {
    if ($num == 2) {
        return "$num is prime!";
    }
    if ($num == 1 || $num % 2 == 0) {
        return "$num is not prime!";
    }
    for ($i = 3; $i < $num; $i++) {
        if ($num % $i == 0) {
            return "$num is not prime!";
            break;
        }
        continue;
    }
    return "$num is prime!";
}

echo is_prime();
echo "\n";
echo is_prime(43);
echo "\n";

// arguments bu reference
function sum(&$a, $b) {
    $a += $b;
    return $a;
}

$num1 = 123;
// both will output 130
echo sum($num1, 7) . "\n";
echo $num1 . "\n";

// Variable-length argument lists
function print_primes (...$nums) {
    foreach ($nums as $num) {
        echo is_prime($num) . "\n";
    }
}

echo print_primes(1, 3, 34, 10, 32, 13);

// functions with named vriables
function get_user_info ($username, $userID, $userAge = 18,) {
    return $username . " with userID $userID, is $userAge years old.";
}
echo get_user_info(userID: 12345, userAge: 22, username:"farhan");
echo "\n";

// variable functions
$favFunc = "print_primes";
echo $favFunc(7, 10, 15);

// anonymous functions
$power = function ($x, $y) {
    $result = 1;
    for ($i = 1; $i <= $y; $i++) {
        $result *= $x;
    }
    return $result;
};

echo $power(2, 11);
echo "\n";

// arrow functions
$powerTwo = fn($x = 2) : float => $x * $x;
echo $powerTwo(50);
echo "\n";

// -----------------------------------------------------
// Class and properties and inheritance
class Car {
    public $carModel = "Super Sport";
    private $ring = "Alominium";
    function __construct ($name, $color) {
        $this->name = $name;
        $this->color = $color;
    }
}

class Lamborghini extends Car {
    public function get_max_speed ($speed) {
        return $speed;
    }
}

$newCar = new Lamborghini("Aventador", "midnight blue");
echo $newCar->carModel;
echo "\n";
// it will show undefined because of its private access modifier
echo $newCar->ring;
echo "\n";
echo $newCar->name;
echo "\n";
echo $newCar->get_max_speed(334);
echo "\n";
?>