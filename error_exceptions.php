<?php
// set a costum error handler
function my_err_handler($errno, $errstr, $errfile, $errline) {
    echo "Error caught! [$errno] $errstr on line $errline \n";
}

set_error_handler("my_err_handler");

echo $a;

// try-catch-finally
function divide($dividend, $divisor) {
  if($divisor == 0) {
    throw new Exception("Division by zero");
  }
  return $dividend / $divisor;
}

try {
    echo divide(5, 0);
} catch(Exception $e) {
    echo "Unable to divide.";
} finally {
    echo "\nFinally Block!";
}

echo "\n";
echo 2;
echo "\n";

// Generator
function getRange ($max = 10) {
  for ($i = 1; $i <= $max; $i++) {
    yield $i;
  }
}

foreach(getRange(7) as $index) {
  echo "Number $index\n";
}

// Generator with key value pairs
function powerTwo ($max = 5) {
  for ($i = 0; $i <= $max; $i++) {
    $powerTwo = $i * $i;
    yield $i => $powerTwo;
  }
}

foreach(powerTwo() as $k => $v) {
  echo "$k to power of two is $v\n";
}

// Attributes
#[communicationAttribute("Hello user", 22)]
class Connect {}

#[Attribute]
class CommunicationAttribute {
  private string $message;
  private string $answer;
  public function __construct (string $message, string $answer) {
    $this->message = $message;
    $this->answer = $answer;
  }
}

$reflector = new \ReflectionClass(Connect::class);
$attrs = $reflector->getAttributes();

foreach($attrs as $attr) {
  var_dump($attr->getName());
  echo "\n";
  var_dump($attr->getArguments());
  echo "\n";
  var_dump($attr->newInstance());
  echo "\n";
}
?>