<?php
    define("JWT", "2345rdfghy65rfgyhgf!");
    echo JWT . "\n";

    class Constants {
        const MY_CONST = 7;

        public static function get_const () {
            return self :: MY_CONST;
        }
    }
    echo Constants::MY_CONST + 3 . "\n";

    $newConst = new Constants();
    echo $newConst->get_const() . "\n";

    $num = Constants::MY_CONST;
    echo $num++ . "\n";
    echo ++$num . "\n";

    // ternary expression
    $name = "Farzan";
    $lastName = $name == "Farhan" ? "Aqaei" : "Unknown";
    echo "How R U $name $lastName?\n";

    echo 100 <=> 100 . "\n";
    echo "\n";
    echo "c" <=> "b" . "\n";
    echo "\n";
    echo [1, 5, 7] <=> [2, 4, 7];
    echo "\n";

    // cast
    echo var_dump((bool) 100);
    echo var_dump((bool) "NULL");

    $null_var = NULL;
    echo $null_var ?? $name . "\n";

    $output = `ls`;
    echo $output;

    $fullName = $name . " " . $lastName;
    echo $fullName . "\n";

    // array concatenation
    $firstArr = ["car" => "Veneno", "volume" => 8000];
    $secondArr = ["car" => "Aventador"];
    $secondArr += $firstArr;
    echo var_dump($secondArr);

    // if statement
    $a = 3;
    $b = 2;
    if ($a < $b):
        echo "a is lesser";
    elseif ($a == $b):
        echo "a is equal to b";
    else :
        echo "b is lesser";
    endif;
    echo "\n";

    // do while loop
    $i = 1;
    do {
        echo $i . "\t";
        $i++;
    } while ($i <= 5);
    echo "\n";

    // for loop
    for ($i = 1; $i < 10; $i++):
        if ($i % 2 == 0) {
            continue;
        }
        if ($i == 9) {
            break;
        }
        echo $i * 2 . "\t";
    endfor;
    echo "\n";

    // return structure
    function getCar($carName) {
        return $carName;
    }

    $theCar = getCar("Aventador");

    // switch case
    switch ($theCar):
        case "Aventador":
            echo "Aventador! good but it is not enough!";
            break;
        case "Veneno":
            echo "Niceeeee!";
            break;
        case "Sian":
            echo "Perfection!";
            break;
        default:
            echo "All of them are nice ones!";
        endswitch;
    echo "\n";
?>